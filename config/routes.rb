FriendCircle::Application.routes.draw do
  root to: "users#show"
  resources :users, only: [:show, :new, :create, :edit, :update] do
    resources :circles
    resources :posts, only: [:new]
    get :feed, to: "users#feed"
  end
  resources :posts, only: [:create, :edit, :update, :destroy, :show]
  resource :reset, only: [:new, :create, :edit, :update]
  resource :session, only: [:new, :create, :destroy]
end
