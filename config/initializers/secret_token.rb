# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FriendCircle::Application.config.secret_key_base = 'eadf5db000b8fac53a4ed0e0b5d580faa1dc596c749fbfc334028b3229416d46058fb10f31ac4ee4af5b0e707cb6aac67b92b00b0826175198fe2793f58cd83c'
