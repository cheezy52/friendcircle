class CreatePostShares < ActiveRecord::Migration
  def change
    create_table :post_shares do |t|
      t.integer :circle_id, :null => false, :references => :circles
      t.integer :post_id, :null => false, :references => :posts

      t.timestamps
    end
  end
end
