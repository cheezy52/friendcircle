class CreateCircleMemberships < ActiveRecord::Migration
  def change
    create_table :circle_memberships do |t|
      t.references :user, :null => false
      t.references :circle, :null => false

      t.timestamps
    end
    add_index :circle_memberships, :user_id, :unique => { :scope => :circle_id }
    add_index :circle_memberships, :circle_id, :unique => { :scope => :user_id }
  end
end
