class RemoveIndicesFromCirclesAndCircleMemberships < ActiveRecord::Migration
  def change
    remove_index :circle_memberships, :user_id
    remove_index :circle_memberships, :circle_id
    remove_index :circles, :name
  end
end
