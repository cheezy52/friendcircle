class ResetsController < ApplicationController
  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.find_by_email(params[:user][:email])
    if @user
      @user.generate_auth_token!
      if @user.save
        msg = PwReset.reset_email(@user)
        msg.deliver!
        flash[:notice] = "Password reset request sent.  Please check your e-mail."
        redirect_to new_session_url
      else
        flash.now[:errors] = @user.errors.full_message
        render :new
      end
    else
      flash.now[:errors] = "No user on file with this e-mail address."
      @user = User.new
      render :new
    end
  end

  def edit
    @user = User.find_by_auth_token(params[:auth_token])
    if @user
      render :edit
    else
      flash[:errors] = "Password reset request not authorized.  Make sure you are using the URL sent from the most recent e-mail."
      redirect_to new_session_url
    end
  end

  def update
    @user = User.find_by_auth_token(params[:auth_token])
    if @user
      @user.password = params[:password]
      if @user.save
        @user.generate_auth_token!
        @user.save!
        render :show
      else
        flash.now[:errors] = @user.errors.full_messages
        render :edit
      end
    else
      flash[:errors] = "Password reset request not authorized.  Only one password reset per e-mail."
      redirect_to new_session_url
    end
  end
end
