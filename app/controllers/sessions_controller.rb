class SessionsController < ApplicationController
  def new
    @user = User.new
    #bluh
    @post = Post.new
    render :new
  end

  def create
    @user = User.find_by_credentials(creds)
    if @user
      @user.login!
      session[:session_token] = @user.session_token
      redirect_to root_url
    else
      flash.now[:errors] = "No user found matching this email/password combo."
      @user = User.new
      render :new
    end
  end

  def destroy
    current_user.generate_session_token!
    session[:session_token] = nil
    redirect_to root_url
  end

  def creds
    params.require(:user).permit(:email, :password)
  end
end
