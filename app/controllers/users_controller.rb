class UsersController < ApplicationController
  before_action :set_user, only: [:edit, :update]
  before_action :verify_user, only: [:edit, :update]

  def show
    if current_user
      if params[:id]
        @user = User.find(params[:id])
        render :show if @user.id == current_user.id
      else
        @user = current_user
        render :show
      end
    else
      redirect_to new_session_url
    end
  end

  def new
    @user = User.new
    @post = @user.owned_posts.build
    3.times { @post.links.build }
    render :new
  end

  def create
    @user = User.new(user_params)
    #we need some talcum powder up in here
    #enjoy this horrible hacky bullshit
    @post = @user.owned_posts.new(post_params) unless post_params.empty?
    @links = @post.links.new(link_params) if @post
    if @user.save
      @user.login!
      session[:session_token] = @user.session_token
      flash[:notice] = "Account created.  Welcome, #{@user.email}!"
      redirect_to root_url
    else
      3.times { @post.links.build }
      flash.now[:errors] = @user.errors.full_messages
      render :new
    end
  end

  def edit
    render :edit
  end

  def update
    if @user && @user.is_password?(params[:user][:password])
      @user.password = params[:user][:new_password]
      if @user.save
        flash[:notice] = "Password changed!"
        @user.login!
        redirect_to root_url
      else
        flash[:errors] = @user.errors.full_messages
        render :edit
      end
    elsif @user
      flash[:errors] = "Old password incorrect."
      ender :edit
    else
      redirect_to new_session_url
    end
  end

  def feed
    @posts = []
    current_user.circles.each do |circle|
      @posts += circle.posts
    end
    @posts.uniq!
    @user = current_user
    render :feed
  end

  def user_params
    params.require(:user).permit(:email, :password)
  end

  def post_params
    params.require(:post).permit(:body)
          .reject { |key, val| val.blank? }
  end

  def link_params
    params.permit(:links => [:url, :title])
          .require(:links)
          .values
          .reject { |data| data.values.all?(&:blank?) }
  end

  def set_user
    @user = User.find(params[:id])
  end
end
