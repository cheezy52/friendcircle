class ApplicationController < ActionController::Base
  include ApplicationHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def verify_user
    check_logged_in
    redirect_to user_url(current_user) unless @user.id == current_user.id
  end

  def check_logged_in
    redirect_to new_session_url unless logged_in?
  end

  def logged_in?
    !current_user.nil?
  end
end
