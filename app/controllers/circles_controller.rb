class CirclesController < ApplicationController
  before_action :set_user
  before_action :verify_user

  def index
    render :index
  end

  def show
    @circle = Circle.find(params[:id])
    render :show
  end

  def new
    @circle = @user.circles.build
    render :new
  end

  def create
    @circle = @user.owned_circles.new(circle_params)
    #@circle.circle_memberships.new(users_params)
    if @circle.save
      flash[:notice] = "Circle #{@circle.name} created!"
      redirect_to user_circles_url(current_user)
    else
      flash.now[:errors] = @circle.errors.full_messages
      render :new
    end
  end

  def edit
    @circle = Circle.find(params[:id])
    render :edit
  end

  def update
    @circle = Circle.find(params[:id])
    @circle.update_attributes(circle_params)
    if @circle.save
      flash[:notice] = "Circle #{@circle.name} updated!"
      redirect_to user_circle_url(current_user, @circle)
    else
      flash.now[:errors] = @circle.errors.full_messages
      render edit
    end
  end

  def destroy
    @circle = Circle.find(params[:id])
    @circle.destroy!
    redirect_to user_circles_url(current_user)
  end

  def users_params
    params.require(:circle)
          .permit(:users => {:user_ids => []})
          .values
  end

  def circle_params
    params.require(:circle).permit(:name, :user_ids => [])
  end

  def set_user
    @user = User.find(params[:user_id])
  end
end
