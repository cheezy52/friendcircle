class PostsController < ApplicationController
  before_action :check_logged_in
  before_action :set_user
  before_action :verify_ownership, only: [:edit, :update, :destroy]

  def new
    @post = current_user.owned_posts.build
    3.times { @post.links.build }
    render :new
  end

  def edit
    @links = @post.links
    render :edit
  end

  def update
    @post.update_attributes(post_params)
    @links = @post.links.map { |link| link.update(link_params.first) }
    @post.circle_ids = params.require(:post).permit(:circle_ids => []).values.flatten
    if @post.save
      flash[:notice] = "Post updated!"
      redirect_to post_url(@post)
    else
      flash.now[:errors] = @post.errors.full_messages
      render :edit
    end
  end

  def create
    @post = @user.owned_posts.new(post_params)
    @links = @post.links.new(link_params)
    @post.circle_ids = params.require(:post).permit(:circle_ids => []).values.flatten
    if @post.save
      flash[:notice] = "Post created!"
      redirect_to post_url(@post)
    else
      flash.now[:errors] = @post.errors.full_messages
      (3 - @post.links.count).times {@post.links.build}
      render :new
    end
  end

  def destroy
    @post ||= Post.find(params[:id])
    @post.destroy!
    redirect_to user_url(@user)
  end

  def show
    #Todo: only allow showing for those with whom post is shared
    @post = Post.find(params[:id])
    render :show
  end


  def post_params
    params.require(:post).permit(:body, :user_id)
  end

  def link_params
    params.permit(:links => [:url, :title])
          .require(:links)
          .values
          .reject { |data| data.values.all?(&:blank?) }
  end

  def set_user
    @user = current_user
  end

  def verify_ownership
    @post ||= Post.find(params[:id])
    redirect_to user_url(current_user) unless @post.user_id == @user.id
  end
end
