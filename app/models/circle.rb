# == Schema Information
#
# Table name: circles
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  owner_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class Circle < ActiveRecord::Base
  validates :name, :presence => true, :uniqueness => {scope: :owner_id}
  validates :owner, :presence => true

  has_many :circle_memberships, :inverse_of => :circle
  has_many :users, through: :circle_memberships

  has_many :post_shares, :inverse_of => :circle
  has_many :posts, :through => :post_shares, :source => :post

  belongs_to :owner, class_name: "User", inverse_of: :owned_circles
end
