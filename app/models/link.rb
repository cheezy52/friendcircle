# == Schema Information
#
# Table name: links
#
#  id         :integer          not null, primary key
#  url        :string(255)      not null
#  post_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class Link < ActiveRecord::Base
  validates :url, :presence => true
  validates :post, :presence => true
  validates :title, :presence => true

  belongs_to :post, :inverse_of => :links
end
