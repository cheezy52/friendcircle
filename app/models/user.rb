# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string(255)      not null
#  password_digest :string(255)      not null
#  session_token   :string(255)
#  authenticated   :boolean
#  auth_token      :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class User < ActiveRecord::Base
  validates :email, :password_digest, :presence => true
  validates :email, :session_token, :auth_token, :uniqueness => true
  validates :password, :length => { :minimum => 6, :allow_nil => true }
  before_validation :ensure_session_token
  before_validation :ensure_auth_token
  before_validation :auth_on_create

  has_many :circle_memberships
  has_many :circles, through: :circle_memberships
  has_many :owned_circles, class_name: "Circle", foreign_key: :owner_id, inverse_of: :owner
  has_many :owned_posts, class_name: "Post", inverse_of: :user
  has_many :post_shares
  has_many :shared_posts, through: :post_shares, source: :post

  def password
    @password
  end

  def password=(plaintext)
    @password = plaintext
    self.password_digest = BCrypt::Password.create(plaintext)
  end

  def is_password?(plaintext)
    BCrypt::Password.new(self.password_digest).is_password?(plaintext)
  end

  def self.find_by_credentials(params)
    user = self.find_by_email(params[:email])
    if user && user.is_password?(params[:password])
      return user
    end
    nil
  end

  def generate_session_token!
    self.session_token = SecureRandom::urlsafe_base64(16)
  end

  def generate_auth_token!
    self.auth_token = SecureRandom::urlsafe_base64(16)
  end

  def reset_password!
    temp_pw = SecureRandom::urlsafe_base64(8)
    self.password_digest = BCrypt::Password.create(temp_pw)
    self.save!
    temp_pw
  end

  def login!
    self.generate_session_token!
    self.save
  end

  def authenticate!
    self.write_attribute(:authenticated, true)
  end

  def ensure_session_token
    self.session_token ||= SecureRandom::urlsafe_base64(16)
  end

  def ensure_auth_token
    self.auth_token ||= SecureRandom::urlsafe_base64(16)
  end

  def auth_on_create
    self.authenticated ||= true
  end
end
