# == Schema Information
#
# Table name: post_shares
#
#  id         :integer          not null, primary key
#  circle_id  :integer          not null
#  post_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class PostShare < ActiveRecord::Base
  validates :circle, :presence => true
  validates :post, :presence => true

  belongs_to :circle, :inverse_of => :post_shares
  belongs_to :post, :inverse_of => :post_shares
end
