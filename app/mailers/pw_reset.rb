class PwReset < ActionMailer::Base
  default from: "from@example.com"

  def reset_email(user)
    @user = user
    @url = "localhost:3000/reset/edit?auth_token=#{@user.auth_token}"
    mail(to: @user.email, subject: "Reset your password")
  end
end
